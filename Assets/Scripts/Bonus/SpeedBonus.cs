﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBonus : MonoBehaviour
{
    public float reloadTime = 15f;
    private float _reloadDeltaTime;
    private bool _isActive;

    private void Start()
    {
        _reloadDeltaTime = 0f;
        _isActive = true;
    }

    private void Update()
    {
        if (_isActive)
            return;
        
        _reloadDeltaTime += Time.deltaTime;
        if (_reloadDeltaTime > reloadTime)
        {
            _isActive = true;
            gameObject.GetComponent<Renderer>().enabled = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (_isActive && other.CompareTag("Player"))
        {
            other.gameObject.GetComponent<PlayerBonusManager>().TriggerSpeedBonus();
            _isActive = false;
            _reloadDeltaTime = 0;
            gameObject.GetComponent<Renderer>().enabled = false;
        }
    }
}
