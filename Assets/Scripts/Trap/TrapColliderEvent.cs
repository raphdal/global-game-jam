﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapColliderEvent : MonoBehaviour
{
    private ParticleSystem _particle;
    private AudioSource _audio;
    
    void Start()
    {
        _audio = transform.GetComponent<AudioSource>();
        _particle = transform.GetComponentInChildren<ParticleSystem>();
        _particle.Stop();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            _particle.transform.position = other.transform.position;
            _particle.Play();
            _audio.Play();
            other.transform.GetComponent<StateManager>().HitTrap();
        }
    }
}
