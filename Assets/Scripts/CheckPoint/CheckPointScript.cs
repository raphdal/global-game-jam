﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CheckPointScript : MonoBehaviour
{
    public bool isActivated = false;
    public bool isEntry = false;
    public bool isExit = false;
    public string nextScene;
    private GameObject scriptJoueur;
    public Animator animator;

    private AudioSource _audio;
    private bool _isFirst = false;

    private void Awake()
    {
        if (isEntry && isExit)
        {
            Debug.Log("Checkpoint can't be entry and exit.");
            throw new Exception("Checkpoint can't be entry and exit.");
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        _audio = transform.GetComponent<AudioSource>();
        _audio.Stop();
        scriptJoueur = GameObject.FindWithTag("Player");
        animator = GetComponent<Animator>();
        if (isEntry)
        {
            scriptJoueur.GetComponent<StateManager>().SetCheckpoint(gameObject);
            scriptJoueur.GetComponent<StateManager>().InstantToToCp();
            _isFirst = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        animator.SetBool("Activated", isActivated);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (isExit)
        {
            Debug.Log("Changing scene.");
            SceneManager.LoadScene("salut");
        }
        else if (other.CompareTag("Player"))
        {
            if (_isFirst)
            {
                _isFirst = false;
                return;
            }
            else if (!isActivated)
            {
                scriptJoueur.GetComponent<StateManager>().SetCheckpoint(gameObject);
                _audio.Play();
            }
            else
                scriptJoueur.GetComponent<StateManager>().OnCheckpointEnter();

            //GameObject.FindWithTag("Player").GetComponent<PlayerMovement>()._changeLastCP(this);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player") && isActivated)
        {
            scriptJoueur.GetComponent<StateManager>().OnCheckpointExit();
        }
    }

    public void Trigger()
    {
        isActivated = !isActivated;
    }
}
