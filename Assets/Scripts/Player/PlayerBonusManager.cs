﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBonusManager : MonoBehaviour
{
    private StateManager _state;
    private PlayerMovement _movement;

    public float speedBonusTime = 2f;
    public float newHorizontalSpeed = 14f;
    public float normalHorizontalSpeed = 10;
    private float _speedBonusDeltaTime;
    private bool _isSpeedBonusActive;
    
    
    // Start is called before the first frame update
    void Start()
    {
        _state = this.GetComponentInParent<StateManager>();
        _movement = this.GetComponentInParent<PlayerMovement>();
        _speedBonusDeltaTime = 0;
        _isSpeedBonusActive = false;
    }

    // Update is called once per frame
    void Update()
    {
        float deltaTime = Time.deltaTime;
        
        _updateSpeedBonus(deltaTime);
    }

    private void _updateSpeedBonus(float deltaTime)
    {
        if (!_isSpeedBonusActive)
            return;
        
        _speedBonusDeltaTime += deltaTime;

        if (_speedBonusDeltaTime > speedBonusTime)
        {
            _isSpeedBonusActive = false;
            _movement.horizontalSpeed = normalHorizontalSpeed;
        }
        else
        {
            _movement.horizontalSpeed = newHorizontalSpeed;
        }
    }
    
    public void TriggerSpeedBonus()
    {
        _speedBonusDeltaTime = 0;
        _isSpeedBonusActive = true;
    }
}
