﻿using System;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class SmoothCamera : MonoBehaviour
{
    public Transform target;
    public Vector3 offset = Vector3.zero;
    public float smoothness = 0.15f;
    
    private Vector3 velocity = Vector3.zero;

    public float shakeIntensity = .5f;
    public float shakeDecay = .005f;

    private float _currentShakeIntensity;
    private Quaternion _originalRotation;

    private void LateUpdate()
    {
        Vector3 desiredPosition = target.position + offset;
        if (_currentShakeIntensity < 0)
        {
            transform.position = Vector3.SmoothDamp(transform.position, desiredPosition, ref velocity, smoothness);
        }
        else
        {
            transform.position = desiredPosition + _getShakePosition();
            transform.rotation = _getShakeRotation();
            _currentShakeIntensity -= shakeDecay;
        }
    }

    private Vector3 _getShakePosition()
    {
        return Random.insideUnitSphere * _currentShakeIntensity;
    }

    private Quaternion _getShakeRotation()
    {
        return new quaternion(
            _originalRotation.x + Random.Range(-_currentShakeIntensity,_currentShakeIntensity)*.2f,
            _originalRotation.y + Random.Range(-_currentShakeIntensity,_currentShakeIntensity)*.2f,
            _originalRotation.z + Random.Range(-_currentShakeIntensity,_currentShakeIntensity)*.2f,
            _originalRotation.w + Random.Range(-_currentShakeIntensity,_currentShakeIntensity)*.2f);
    }

    public bool IsShaking()
    {
        return _currentShakeIntensity > 0;
    }
    
    public void TriggerShake()
    {
        _originalRotation = transform.rotation;
        _currentShakeIntensity = shakeIntensity;
    }

    private void Update()
    {
        float time = GameObject.Find("Player").GetComponent<StateManager>().TimeRemaining();
        this.GetComponentInChildren<Text>().text = ((int)time).ToString();
    }
}