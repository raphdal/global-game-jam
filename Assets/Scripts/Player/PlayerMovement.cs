﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerMovement : MonoBehaviour
{
    public enum State
    {
        Grounded,
        Ascending,
        Falling,
        TimedOut,
    }
    
    public float horizontalSpeed = 5;
    public float jumpForce = 5;
    public float longJumpMultiplier = 350f;
    
    private Vector2 _speed;
    private Vector2 _movement;

    private State _state;
    public float jumpDelay = 0.1f;
    private float _jumpDeltaTime;
    private float _jumpTimeCounter;
    public float _jumpTime;
    public Animator animator;

    private Rigidbody2D _body;

    // Start is called before the first frame update
    void Start()
    {
        _speed = Vector2.zero;
        _movement = Vector2.zero;
        _body = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_state == State.TimedOut)
            return;
        animator.SetFloat("Horizontal", Input.GetAxisRaw("Horizontal"));
        animator.SetBool("Grounded", _state == State.Grounded);
        _updateState();
        _defineBasicMovement();
        _applyBasicSpeed(Time.deltaTime);
    }

    private void _updateState()
    {
        RaycastHit2D[] hits = Physics2D.RaycastAll(_body.position, Vector2.down, 2.1f);
        Debug.DrawRay(_body.position, Vector2.down * 1.1f, Color.red);

        Debug.Log("State: " + _state);
        if (hits.Length > 0)
        {
            foreach (RaycastHit2D hit in hits)
            {
                if (hit.transform.gameObject.tag == "Ground" || hit.transform.gameObject.tag == "Platform")
                {
                    _state = State.Grounded;
                    return;
                }
            }
        }
        if (_body.velocity.y > 0)
            _state = State.Ascending;
        else if (_body.velocity.y < 0)
            _state = State.Falling;
    }

    private void _defineBasicMovement()
    {
        _movement.x = Input.GetAxisRaw("Horizontal");
        _speed.x = horizontalSpeed;
        _jumpDeltaTime += Time.deltaTime;

        if (_state == State.Grounded && _jumpDeltaTime > jumpDelay && Input.GetAxisRaw("Vertical") > 0)
        {
            _body.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            _jumpDeltaTime = 0;
            _jumpTimeCounter = _jumpTime;
        }
        if (Input.GetAxisRaw("Vertical") > 0 && _state != State.Grounded)
        {
            if (_jumpTimeCounter > 0)
            {
                _body.AddForce(Vector2.up * jumpForce * Time.deltaTime * longJumpMultiplier);
                _jumpTimeCounter -= Time.deltaTime;
            } else
            {
                _state = State.Falling;
            }
        } else if (_state == State.Ascending)
        {
            _state = State.Falling;
        }
    }

    public void blockActions(bool block)
    {
        if (block)
        {
            _state = State.TimedOut;
            GetComponent<BoxCollider2D>().enabled = false;
            GetComponent<Rigidbody2D>().simulated = false;
        }
        else
        {
            _state = State.Falling;
            GetComponent<BoxCollider2D>().enabled = true;
            GetComponent<Rigidbody2D>().simulated = true;
        }
    }

    private void _applyBasicSpeed(float deltaTime)
    {
        _body.velocity = _movement * _speed * Vector2.right + _body.velocity * Vector2.up;
        _movement = Vector2.zero;
        _speed = Vector2.zero;
    }
}
