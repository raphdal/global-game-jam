﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.UI;
using UnityEngine;

public class StateManager : MonoBehaviour
{
    public float maxTime = 2.0f;
    public float speed = 50.0f;

    private float _deltaTime;

    private bool _isCounterActivated;

    private GameObject CurrentCheckpoint;

    private Rigidbody2D _body;

    private SmoothCamera _camera;
    
    void Start()
    {
        _deltaTime = 0;
        _isCounterActivated = false;
        _body = transform.GetComponent<Rigidbody2D>();
        _camera = GameObject.Find("Main Camera").GetComponent<SmoothCamera>();
    }

    void Update()
    {
        if (_isCounterActivated)
        {
            _deltaTime += Time.deltaTime;
            if (_deltaTime > maxTime)
                TpToLastCp();
        }
    }

    public void OnCheckpointExit()
    {
        _isCounterActivated = true;
        this.GetComponent<PlayerMovement>().blockActions(false);
        _deltaTime = 0;
    }

    public void SetCheckpoint(GameObject checkpoint)
    {
        if (CurrentCheckpoint != null)
            CurrentCheckpoint.GetComponentInChildren<CheckPointScript>().Trigger();
        CurrentCheckpoint = checkpoint;
        CurrentCheckpoint.GetComponentInChildren<CheckPointScript>().Trigger();
        OnCheckpointEnter();
    }

    public void OnCheckpointEnter()
    {
        _isCounterActivated = false;
        _deltaTime = 0;
        this.GetComponent<PlayerMovement>().blockActions(false);
    }
    
    public void HitTrap()
    {
        ClearForces();
        _camera.TriggerShake();
        StartCoroutine("TrapAnnimationCoroutine");
    }

    private IEnumerator TrapAnnimationCoroutine()
    {
        Debug.Log("Starting coroutine.");
        this.GetComponent<PlayerMovement>().blockActions(true);
        gameObject.GetComponent<Renderer>().enabled = false;
        while (_camera.IsShaking())
        {
            yield return new WaitForSeconds(0.1f);
        }

        gameObject.GetComponent<Renderer>().enabled = true;
        this.GetComponent<PlayerMovement>().blockActions(false);
        Debug.Log("End of coroutine.");
        transform.position = CurrentCheckpoint.transform.position + new Vector3(0.0f, 1.5f, 0.0f);
    }

    public void InstantToToCp()
    {
        ClearForces();
        transform.position = CurrentCheckpoint.transform.position + new Vector3(0.0f, 1.5f, 0.0f);
    }

    public void TpToLastCp()
    {
        ClearForces();
        this.GetComponent<PlayerMovement>().blockActions(true);
        if (transform.position != CurrentCheckpoint.transform.position)
        {
            transform.position = Vector2.MoveTowards(transform.position, CurrentCheckpoint.transform.position + new Vector3(0.0f, 1.5f, 0.0f), speed * Time.deltaTime);
        } else
        {
            transform.position = CurrentCheckpoint.transform.position + new Vector3(0.0f, 1.5f, 0.0f);
        }
        this.GetComponent<PlayerMovement>().blockActions(false);
    }

    public void ClearForces()
    {
        _body.angularVelocity = 0;
        _body.velocity = Vector2.zero;
    }

    public float TimeRemaining()
    {
        return maxTime - _deltaTime;
    }
}
